package com.jumiapay.loggerproject.repository;

import com.jumiapay.loggerproject.model.ActivityLogger;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.time.LocalDateTime;


public interface LoggerRepository extends MongoRepository<ActivityLogger, String> {
    Page<ActivityLogger> findByWho(String email, Pageable pageable);
    Page<ActivityLogger> findByWhoAndWhenBetween(String email, LocalDateTime start, LocalDateTime stop, Pageable pageable);
    Page<ActivityLogger> findByWhenBetween(LocalDateTime start, LocalDateTime stop, Pageable pageable);
}
