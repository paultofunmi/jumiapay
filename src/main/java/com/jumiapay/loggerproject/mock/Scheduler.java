package com.jumiapay.loggerproject.mock;

import com.github.javafaker.Faker;
import com.jumiapay.loggerproject.classes.CreditCard;
import com.jumiapay.loggerproject.classes.User;
import com.jumiapay.loggerproject.model.ActivityLogger;
import com.jumiapay.loggerproject.service.ActivityLoggerSender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import java.time.LocalDateTime;


@Component
public class Scheduler {

    Faker faker;

    @Autowired
    ActivityLoggerSender activityLoggerSender;

    private static final Logger clogger = LoggerFactory.getLogger(Scheduler.class);

    Scheduler (){
        faker = new Faker();
    }

    /**
     * Simulate a producer.
     * It creates 3 activity logs with dates in the past to test start and stop date in query params.
     */
    @Scheduled(fixedRate = 10000)
    public void createUser() {
        User user = new User.UserBuilder(faker.name().firstName(), "123456", faker.internet().emailAddress())
                            .creditCard(new CreditCard(faker.finance().creditCard(), faker.number().digits(3), faker.number().digits(4)))
                            .lastName(faker.name().lastName())
                            .build();

        activityLoggerSender.sendLog(new ActivityLogger("User created", user.toString(), LocalDateTime.of(2011, 11, 23, 11, 00), user.getEmail()));
        activityLoggerSender.sendLog(new ActivityLogger("User created", user.toString(), LocalDateTime.of(2012, 11, 23, 11, 00), user.getEmail()));
        activityLoggerSender.sendLog(new ActivityLogger("User created", user.toString(), LocalDateTime.of(2013, 11, 23, 11, 00), user.getEmail()));
    }

    /**
     * Simulates a producer that creates a user and changes the password
     */
    @Scheduled(fixedRate = 5000)
    public void createUserAndChangePassword() {
        User user = new User.UserBuilder(faker.name().firstName(), "123456", faker.internet().emailAddress())
                .creditCard(new CreditCard(faker.number().digits(16), faker.number().digits(3), faker.number().digits(4)))
                .lastName(faker.name().lastName())
                .build();
        activityLoggerSender.sendLog(new ActivityLogger("User created", user.toString(), LocalDateTime.now(), user.getEmail()));

        user.setPassword("54321");
        activityLoggerSender.sendLog(new ActivityLogger("Password Updated", user.toString(), LocalDateTime.now(), user.getEmail()));
    }

    /**
     * Simulates a producer that creates a user and adds a credit card to the user
     */
    @Scheduled(fixedRate = 7000)
    public void createUserAndCreditCard() {
        User user = new User.UserBuilder( faker.name().firstName(), "123456", faker.internet().emailAddress())
                .lastName(faker.name().lastName())
                .build();

        activityLoggerSender.sendLog(new ActivityLogger("User created", user.toString(), LocalDateTime.now(), user.getEmail()));

        user.setCreditCard((new CreditCard(faker.finance().creditCard(), faker.number().digits(3), faker.number().digits(4))));

        activityLoggerSender.sendLog(new ActivityLogger("Credit card added", user.toString(), LocalDateTime.now(), user.getEmail()));

    }
}
