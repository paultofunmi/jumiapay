package com.jumiapay.loggerproject.model;

import com.jumiapay.loggerproject.classes.ILogger;
import com.jumiapay.loggerproject.classes.User;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.time.LocalDateTime;

@Document
public class ActivityLogger implements ILogger, Serializable {

	@Id
	private String id;
	private String what;
	private String description;
	private LocalDateTime when;
	private String who;


	public ActivityLogger(String what, String description, LocalDateTime when, String who) {
		this.what = what;
		this.description = description;
		this.when = when;
		this.who = who;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getWhat() {
		return what;
	}

	public void setWhat(String what) {
		this.what = what;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public LocalDateTime getWhen() {
		return when;
	}

	public void setWhen(LocalDateTime when) {
		this.when = when;
	}

	public String getWho() {
		return who;
	}

	public void setWho(String who) {
		this.who = who;
	}

	@Override
	public LocalDateTime when() {
		return this.when;
	}

	@Override
	public String what() {
		return this.what;
	}

	@Override
	public String who() {
		return this.who;
	}

	@Override
	public String toString() {
		return "ActivityLogger{" +
				"id='" + id + '\'' +
				", what='" + what + '\'' +
				", description='" + description + '\'' +
				", when=" + when +
				", who='" + who + '\'' +
				'}';
	}
}
