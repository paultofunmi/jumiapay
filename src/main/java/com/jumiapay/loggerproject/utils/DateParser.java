package com.jumiapay.loggerproject.utils;

import java.time.LocalDateTime;

public class DateParser {


    public static LocalDateTime getDate(String date){
        String[] startArry = date.split("-");
        return LocalDateTime.of(Integer.valueOf(startArry[0]), Integer.valueOf(startArry[1]), Integer.valueOf(startArry[2]), 00, 01);
    }
}
