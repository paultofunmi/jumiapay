package com.jumiapay.loggerproject.service;

import com.jumiapay.loggerproject.model.ActivityLogger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;


@Service
public class ActivityLoggerSender {

    private RabbitTemplate rabbitTemplate;

    private static final Logger clogger = LoggerFactory.getLogger(ActivityLoggerSender.class);

    @Autowired
    LoggerService loggerService;

    @Value("${rabbitmq.routing_key}")
    private String routing_key;

    @Value("${rabbitmq.exchange}")
    private String exchange_name;


    public ActivityLoggerSender(final RabbitTemplate rabbitTemplate){
        this.rabbitTemplate = rabbitTemplate;
    }

    public void sendLog(ActivityLogger activityLogger){
        rabbitTemplate.convertAndSend(exchange_name, routing_key, activityLogger);
    }
}
