package com.jumiapay.loggerproject.service;


import com.jumiapay.loggerproject.model.ActivityLogger;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


public interface LoggerService {

	public Page<ActivityLogger> findAll(Pageable pageable);

	public Page<ActivityLogger> findAll(Pageable pageable, String start, String stop);

	public ActivityLogger save(ActivityLogger logger);

	public Page<ActivityLogger> findByEmail(String id, Pageable pageable, String start, String stop);

	public Page<ActivityLogger> findByEmail(String id, Pageable pageable);
}
