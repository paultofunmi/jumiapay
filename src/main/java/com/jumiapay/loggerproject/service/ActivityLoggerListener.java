package com.jumiapay.loggerproject.service;

import com.jumiapay.loggerproject.model.ActivityLogger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class ActivityLoggerListener {

    @Autowired
    LoggerService loggerService;

    private static final Logger clogger = LoggerFactory.getLogger(ActivityLoggerListener.class);

    private final String DEFAULT_PARSING_QUEUE = "logger_parser_q";

    @RabbitListener(queues = DEFAULT_PARSING_QUEUE)
    public void consumeDefaultMessage(final ActivityLogger message){
        loggerService.save(message);
    }
}
