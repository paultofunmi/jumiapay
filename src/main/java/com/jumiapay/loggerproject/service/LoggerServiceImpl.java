package com.jumiapay.loggerproject.service;


import com.jumiapay.loggerproject.model.ActivityLogger;
import com.jumiapay.loggerproject.repository.LoggerRepository;
import com.jumiapay.loggerproject.utils.DateParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class LoggerServiceImpl implements  LoggerService{


	@Autowired
	LoggerRepository loggerRepository;

	@Override
	public Page<ActivityLogger> findAll(Pageable pageable) {
		return loggerRepository.findAll(pageable);
	}

	@Override
	public Page<ActivityLogger> findAll(Pageable pageable, String start, String stop) {
		return loggerRepository.findByWhenBetween(DateParser.getDate(start), DateParser.getDate(stop), pageable);
	}

	@Override
	public ActivityLogger save(ActivityLogger logger) {
		return loggerRepository.save(logger);
	}

	@Override
	public Page<ActivityLogger> findByEmail(String email, Pageable pageable, String start, String stop) {
		return loggerRepository.findByWhoAndWhenBetween(email, DateParser.getDate(start), DateParser.getDate(stop), pageable);
	}

	@Override
	public Page<ActivityLogger> findByEmail(String email, Pageable pageable) {
		return loggerRepository.findByWho(email, pageable);
	}
}
