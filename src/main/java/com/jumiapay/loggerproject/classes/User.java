package com.jumiapay.loggerproject.classes;

import at.favre.lib.crypto.bcrypt.BCrypt;

public class User {

    private String id;
    private String firstName;
    private String lastName;
    private String email;
    private String password;
    private CreditCard creditCard;

    private User() {}

    private User(UserBuilder userBuilder) {
        this.firstName = userBuilder.firstName;
        this.lastName = userBuilder.lastName;
        this.email = userBuilder.email;
        this.creditCard = userBuilder.creditCard;
        this.password = userBuilder.password;

    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return BCrypt.withDefaults().hashToString(5, this.password.toCharArray());
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public CreditCard getCreditCard() {
        return creditCard;
    }

    public void setCreditCard(CreditCard creditCard) {
        this.creditCard = creditCard;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append(" firstName: ");
        sb.append(this.firstName);

        if(this.getLastName() != null){
            sb.append(" lastName: ");
            sb.append(this.lastName );
        }

        sb.append(" email: ");
        sb.append(this.email );
        sb.append(" password: ");
        sb.append(this.getPassword() );

        if(this.getCreditCard() != null){
            sb.append(" CreditCard: ");
            sb.append(this.getCreditCard() );
        }

        return sb.toString();
    }



    public static class UserBuilder{

        private String id;
        private String firstName;
        private String lastName;
        private String email;
        private String password;
        private CreditCard creditCard;

        public UserBuilder(String firstName, String password, String email){
            this.firstName = firstName;
            this.password = password;
            this.email = email;
        }

        public UserBuilder firstName(String firstName){
            this.firstName = firstName;
            return this;
        }

        public UserBuilder lastName(String lastName){
            this.lastName = lastName;
            return this;
        }

        public UserBuilder email(String email){
            this.email = email;
            return this;
        }

        public UserBuilder password(String password){
            this.password = password;
            return this;
        }
        public UserBuilder creditCard(CreditCard creditCard){
            this.creditCard = creditCard;
            return this;
        }

        public User build(){
            return new User(this);
        }
    }


}
