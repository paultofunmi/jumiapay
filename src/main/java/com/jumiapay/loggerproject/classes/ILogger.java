package com.jumiapay.loggerproject.classes;

import java.time.LocalDateTime;

public interface ILogger {

    LocalDateTime when();

    String what();

    String who();

}
