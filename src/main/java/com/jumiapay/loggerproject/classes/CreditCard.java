package com.jumiapay.loggerproject.classes;

import at.favre.lib.crypto.bcrypt.BCrypt;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class CreditCard {

    private LocalDateTime expiryDate;

    private String cardNumber;

    private String cvc;

    private String pin;

    private LocalDateTime issuedOn;

    public CreditCard() { }

    public CreditCard(String cardNumber, String cvc, String pin) {
        this.cvc = cvc;
        this.pin = pin;
        this.setCardNumber(cardNumber);
        this.setIssuedOn();
        this.setExpiryDate();
    }

    public LocalDateTime getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate() {
        //Card expires 5 years after creation
        this.expiryDate = this.getIssuedOn().toLocalDate().atTime(11,59,59).plusYears(5L);
    }

    public String getCardNumber() {
        /**
         * Masking Card
         */
        return CreditCard.maskNumber(this.cardNumber, '*');
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getCvc() {
        return cvc;
    }

    public void setCvc(String cvc) {
        this.cvc = cvc;
    }

    public String getPin() {
        return BCrypt.withDefaults().hashToString(5, this.pin.toCharArray());
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public LocalDateTime getIssuedOn() {
        return issuedOn;
    }

    public void setIssuedOn() {
        this.issuedOn = LocalDate.now().atStartOfDay();
    }

    public static String maskNumber(String number, char mask) {
        char[] numArray = number.toCharArray();
        StringBuilder b = new StringBuilder();

        if(numArray.length == 16){
            //regular cards: MasterCard and VISA have 16 digits
            int startIndex = 4;
            int stopIndex = 12;

            for(int i = startIndex; i < stopIndex; i++){
                numArray[i] = mask;
            }

            return b.append(numArray).toString();
        }else if(numArray.length == 19){
            //Verve cards = 19 digits.
            int startIndex = 4;
            int stopIndex = 15;

            for(int i = startIndex; i < stopIndex; i++){
                numArray[i] = mask;
            }
            return b.append(numArray).toString();
        }else {
            return null;
        }
    }

    @Override
    public String toString() {
        return "CreditCard{" +
                "expiryDate=" + expiryDate +
                ", cardNumber='" + this.getCardNumber() + '\'' +
                ", cvc=" + cvc +
                ", pin=" + this.getPin() +
                ", issuedOn=" + issuedOn +
                '}';
    }
}
