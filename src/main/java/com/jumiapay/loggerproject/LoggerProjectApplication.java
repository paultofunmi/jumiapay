package com.jumiapay.loggerproject;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class LoggerProjectApplication {

	@Value("${rabbitmq.exchange}")
	private String exchange_name;

	@Value("${rabbitmq.parsing_queue}")
	private String default_parsing_queue;

	@Value("${rabbitmq.routing_key}")
	private String routing_key;

	public static void main(String[] args) {
		SpringApplication.run(LoggerProjectApplication.class, args);
	}

	@Bean
	public TopicExchange tipsExchange(){
		return new TopicExchange(exchange_name);
	}

	@Bean
	public Queue defaultParsingQueue(){
		return new Queue(default_parsing_queue);
	}
	@Bean
	public Binding queueToExchangeBinding(){
		return BindingBuilder.bind(defaultParsingQueue()).to(tipsExchange()).with(routing_key);
	}

}
