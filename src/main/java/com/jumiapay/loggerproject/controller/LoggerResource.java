package com.jumiapay.loggerproject.controller;

import com.jumiapay.loggerproject.model.ActivityLogger;
import com.jumiapay.loggerproject.service.LoggerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(value = "/api/", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
public class LoggerResource {

    @Autowired
    LoggerService loggerService;

    @GetMapping("/logs")
    public Iterable<ActivityLogger> getAllWithinRange(Pageable pageable,
                                                      @RequestParam(value="start", required = false) String start,
                                                      @RequestParam(value="stop", required = false) String stop) {

        if(start != null && stop != null){
            return loggerService.findAll(pageable, start, stop);
        }else {
            return loggerService.findAll(pageable);
        }
    }

    @GetMapping("/logs/user")
    public Iterable<ActivityLogger> getByUserWithinRange(@RequestParam(value = "email") String email, Pageable pageable,
                                                         @RequestParam(value="start", required = false) String start,
                                                         @RequestParam(value="stop", required = false) String stop) {

        if(start != null && stop != null){
            return loggerService.findByEmail(email, pageable, start, stop);
        }else {
            return loggerService.findByEmail(email, pageable);
        }

    }



}