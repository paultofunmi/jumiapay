package com.jumiapay.loggerproject.classes;

import at.favre.lib.crypto.bcrypt.BCrypt;
import com.github.javafaker.Faker;
import org.junit.Assert;
import static org.hamcrest.CoreMatchers.containsString;
import org.junit.Test;

public class CreditCardTest {
    @Test
    public void testCreditCardIsNotNull() {
        CreditCard creditCard = new CreditCard("cardNumber", "cvc", "pin");

        Assert.assertNotNull(creditCard);
    }

    @Test
    public void testCardNumberIsNull() {
        CreditCard creditCard = new CreditCard("cardNumber", "cvc", "pin");
        Assert.assertNull("Invalid card number", creditCard.getCardNumber());
    }

    @Test
    public void testInvalidCardNumber() {
        Faker faker = new Faker();
        CreditCard creditCard = new CreditCard(faker.number().digits(10), faker.number().digits(3), faker.number().digits(4));
        Assert.assertNull("Invalid Card number", creditCard.getCardNumber());
    }

    @Test
    public void test16DigitsCardNumberIsMasked() {
        Faker faker = new Faker();
        CreditCard creditCard = new CreditCard(faker.number().digits(16), faker.number().digits(3), faker.number().digits(4));
        char[] charArray = creditCard.getCardNumber().substring(4,11).toCharArray();
        Assert.assertArrayEquals("Card is not masked", charArray, creditCard.getCardNumber().substring(4,11).toCharArray());
    }

    @Test
    public void test19DigitsCardNumberIsMasked() {
        Faker faker = new Faker();
        CreditCard creditCard = new CreditCard(faker.number().digits(19), faker.number().digits(3), faker.number().digits(4));
        char[] charArray = creditCard.getCardNumber().substring(4,14).toCharArray();
        Assert.assertArrayEquals("Verve card is not masked", charArray, creditCard.getCardNumber().substring(4,14).toCharArray());
    }

    @Test
    public void testCreditCardHasMaskedCharacters() {
        Faker faker = new Faker();
        CreditCard creditCard = new CreditCard(faker.number().digits(19), faker.number().digits(3), faker.number().digits(4));
        Assert.assertThat("Password is not masked", creditCard.getCardNumber(), containsString("*"));
    }

    @Test
    public void testPasswordEncryptDecrypt() {
        Faker faker = new Faker();
        String pin = faker.number().digits(4);
        CreditCard creditCard = new CreditCard(faker.number().digits(19), faker.number().digits(3), pin);
        BCrypt.Result result = BCrypt.verifyer().verify(pin.toCharArray(), creditCard.getPin() );
        Assert.assertTrue("Passwords do not match", result.verified);
    }
}
