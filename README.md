# Jumiapay Logger

### Approach to this project
A centralised logger application has been built whereby different services send their activity logs to a central logging application.

### How 1.1
A mock scheduler has been created that publishes messages at different intervals (this is akin to different services generating their logs and sending to central logging application).
A listener (on the central logging application) captures this message and stores them into its database.

### How 1.2
A Credit card class has been created to show masking and it has been assumed that the platform supports the creation of new cards hence the inclusion of issued_on and expiry date fields

### How 1.3
A User class has been created to show masking of pin

### Routes
All logs can be retrieved.
Logs by a particular user can also be retrieved.


#### Route 1 - All logs
localhost:8080/api/logs - retrieves all logs without restriction

#### Route 2 - All logs sorted by an order
localhost:9001/api/logs?sort=when,DESC - retrieves all logs sorted in descending order.
Other options are: sort=id|what|who|description, ASC|DESC

#### Route 3 - All logs within a time period and sorted
localhost:9001/api/logs?start=2019-01-23&stop=2019-12-29&sort=when,DESC

#### Route 4 - All logs by a user
localhost:9001/api/logs/user?email=johnnie.veum@yahoo.com

#### Route 5 - All logs by a user that is sorted and within a time frame
localhost:9001/api/logs/user?email=johnnie.veum@yahoo.com&start=2019-01-23&stop=2019-12-29&sort=when,DESC


#### RabbitMQ Admin Interface
Available at port 15672


#### Going forward
1. I would add Docker for containerisation
2. Add test cases for repositories, services and controllers.



